#!/bin/bash
export hpcode
path="/home/massimiliano/mance";
snmpget -v 2c -c pubrim $1 1.3.6.1.2.1.47.1.1.1.1.13.2 |  sed -n -e 's/^.*\(......\).$/\1/p' &> $path/prod;
hpcode=$(cat $path/prod);
echo $hpcode;
