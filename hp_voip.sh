#!bin/bash
#script name: vet_voip.sh
#This script add voip vlan to Ozzano campus
#Length=/home/massimiliano/Eth_int
script_path1="/home/massimiliano/vet/hp_type.sh"
script_path2="/home/massimiliano/vet/camp.sh"
#script_path3="/home/massimiliano/vet/assets/hp_voip_prod.py"
script_path3="/home/massimiliano/vet/assets/hp_voip_prod2.py"
path="/home/massimiliano/vet/assets"
#the file below collect a list of ip address end uplink interface of switchs
file=/home/massimiliano/vet/assets/vet_ip
COUNT=0
#oid=".1.3.6.1.2.1.2.2.1.2"
#the while below flush the file above and get ip and uplink variables
while read -r line; do
  #rm -f $path/camp*;
  #rm -f $path/Eth_int;
  #rm -f $path/add_camp*;
  #rm -f $path/int*;
  rm -f $path/text_line;
  echo $line | sed -e 's/[^@]*@//' &>> "$path"/prod_code
  ip=$(echo "$line" | sed -e 's/@.*//');
  pcode=$(echo "$line" | sed -e 's/[^@]*@//');
  #uplink=$(echo "$line" | sed -e 's/[^@]*@//');
  """r=0
  for k in $(echo "$line" | tr "@" "\n")
  #for (( k = 1; k < Lengthline; ++ k ));
  do
    if [ $r -lt 1 ] ;then
      ip=$k;
      echo "$ip";
      r=$(expr $r + 1);
    else
      uplink$r=$k;
      #print $uplink
      export uplink$r;
      r=$(expr $r + 1);
    fi
  done
  s=$(expr $r - 1);
  export r"""
  rm -f $path/camp*;
  rm -f $path/Eth_int;
  rm -f $path/add_camp*;
  rm -f $path/int*;
  #next command retrieve interface list by mean of snmp query
  snmpwalk -v 2c -c pubrim $ip .1.3.6.1.2.1.2.2.1.2 | sed -n -e 's/^.*STRING: //p' &>> "$path"/int; 
  #next query retrieve a list of interfaces status ("up" or "down")
  snmpwalk -v 2c -c pubrim $ip .1.3.6.1.2.1.2.2.1.8 | sed -n -e 's/^.*INTEGER: //p' &>> "$path"/int_up; 
  source $script_path1 $pcode;
  #
  file2=$(cat  /home/massimiliano/vet/assets/Eth_int |tr "\n" " ")
  Length1=($file2)
  echo $Length1;
  #fileItem=($file)
  #Length=${#fileItem[@]}
  #path="/home/massimiliano"
  virg=','
  #camp=camp
  for (( i = 0; i < Length1; ++ i ));
  do
    camp=$(cat  /home/massimiliano/vet/assets/camp$i)
    COUNT=$(expr $i + 1)
    if [[ $COUNT < $Length1  ]]; then
      #echo $camp$virg &>>$path/add_camp;
      #the following is a change to set loop protection cmd
      echo $camp &>>$path/add_camp;
    else
      echo $camp &>>$path/add_camp;
    fi
  done
  awk '
  {
      for (i=1; i<=NF; i++)  {
          a[NR,i] = $i
      }
  }
  NF>p { p = NF }
  END {
      for(j=1; j<=p; j++) {
          str=a[1,j]
          for(i=2; i<=NR; i++){
              str=str" "a[i,j];
          }
          print str
      }
  }' $path/add_camp &>> $path/add_camp2
  tr -d ' ' < $path/add_camp2 > $path/add_camp3
  #
  #source $script_path2;
  ports=$(cat /home/massimiliano/vet/assets/add_camp3)
  echo $ports;
  #python script to configure network device
  python $script_path3 $ip $pcode;
done < "$file"
